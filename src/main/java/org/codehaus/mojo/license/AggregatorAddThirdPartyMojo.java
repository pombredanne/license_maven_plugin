package org.codehaus.mojo.license;

/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.codehaus.mojo.license.model.Dependency;
import org.codehaus.mojo.license.model.LicenseMap;
import org.codehaus.mojo.license.model.MavenDependency;
import org.codehaus.mojo.license.model.Scope;
import org.codehaus.mojo.license.utils.SortedProperties;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * This aggregator goal (will be executed only once and only on pom projects)
 * executed the {@code add-third-party} on all his scopesPerModule (in a parellel build cycle)
 * then aggreates all the third-party files in final one in the pom project.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
@Mojo( name = "generate-bom", requiresProject = true, aggregator = true,
       requiresDependencyResolution = ResolutionScope.TEST,
       defaultPhase = LifecyclePhase.GENERATE_RESOURCES )
public class AggregatorAddThirdPartyMojo
    extends AbstractAddThirdPartyMojo
{

    // ----------------------------------------------------------------------
    // Mojo Parameters
    // ----------------------------------------------------------------------

    /**
     * The projects in the reactor.
     *
     * @since 1.0
     */
    @Parameter( property = "reactorProjects", readonly = true, required = true )
    protected List<MavenProject> reactorProjects;

    // ----------------------------------------------------------------------
    // AbstractLicenseMojo Implementaton
    // ----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean checkPackaging()
    {
        return true;
    }



    /**
     * {@inheritDoc}
     */
    @Override
    protected void doAction()
        throws Exception
    {
        Log log = getLog();

        if ( isVerbose() )
        {
            log.info( "After executing on " + reactorProjects.size() + " project(s)" );
        }


        getHelper().removeIgnoredLicenses(licenseMap, getIgnoredLicenses());

        getLog().info( getProjectDependencies().size() + " detected artifact(s)." );
        if ( isVerbose() )
        {
            for ( String id : getProjectDependencies().keySet() )
            {
                getLog().info( " - " + id );
            }
        }

        if ( isVerbose() )
        {
            getLog().info( licenseMap.numberOfLicenses() + " detected license(s)." );
            for ( String id : licenseMap.getAllLicenses() )
            {
                getLog().info( " - " + id );
            }
        }


        boolean safeLicense = checkForbiddenLicenses();


        writeThirdPartyFile();

        if ( isVerbose() )
        {
            StringBuilder infoMessage = new StringBuilder("\n\nScopes of dependencies: \n");
            for (Dependency dependency : licenseMap.getProjects())
            {
                infoMessage.append(String.format(" - %s  (%s)\n", dependency.getArtifactName(), dependency.getLicenseScope().getPrintableName()));
                for ( Map.Entry<String, Scope> entry : dependency.getScopesPerModule().entrySet() )
                {
                    infoMessage.append(String.format("\t%s (%s)\n", entry.getKey(), entry.getValue().getPrintableName()));
                }
                infoMessage.append("\n");

            }

            getLog().info(infoMessage.toString());
        }

        if ( !safeLicense && isFailIfWarning() )
        {
            throw new MojoFailureException( "There is some forbidden licenses used, please check your dependencies." );
        }

        if ( isFailIfWarning() && getLicenseMap().getDependenciesByLicense(LicenseMap.UNKNOWN_LICENSE_MESSAGE).size() > 0 )
        {
            SortedSet<Dependency> unknownDeps = getLicenseMap().getDependenciesByLicense(LicenseMap.UNKNOWN_LICENSE_MESSAGE);
            StringBuilder sb = new StringBuilder();


            Set<Dependency> mavenDeps = new TreeSet<Dependency>();
            Set<Dependency> nonMavenDeps = new TreeSet<Dependency>();
            for ( Dependency dep : unknownDeps )
            {
                if (dep instanceof MavenDependency)
                {
                    mavenDeps.add(dep);
                }
                else
                {
                    nonMavenDeps.add(dep);
                }
            }

            if (!mavenDeps.isEmpty())
            {
                sb.append("\n\nThere " + getHelper().pluralise(mavenDeps.size(), "is ", "are ") + mavenDeps.size());
                sb.append(" maven");
                sb.append(getHelper().pluralise(mavenDeps.size(), " dependency ", " dependencies ")).append("with no license:");
                for ( Dependency dep : mavenDeps )
                {
                    sb.append( "\n - " + dep.getArtifactName() );
                }

                sb.append("\n\nUpdate the file ");
                sb.append(getOverrideFile() != null? getOverrideFile().getAbsolutePath() : "");
                sb.append(" to include the license for each of the components above. \n\n");
            }

            if (!nonMavenDeps.isEmpty())
            {
                sb.append("\n\nThere " + getHelper().pluralise(nonMavenDeps.size(), "is ", "are ") + nonMavenDeps.size());
                sb.append(" non maven");
                sb.append(getHelper().pluralise(nonMavenDeps.size(), " dependency ", " dependencies ")).append("with no license:");

                for ( Dependency dep : unknownDeps )
                {
                    sb.append("\n - " + dep.getArtifactName());
                }
                sb.append("\n\nUpdate the file ");
                sb.append(getNonMavenDependenciesFile() != null? getNonMavenDependenciesFile().getAbsolutePath() : "");
                sb.append(" to include the license for each of the components above. \n\n");
            }




            log.error(sb.toString());



            throw new MojoFailureException( "There is some dependencies with unknown license, please review the modules." );
        }
    }

    // ----------------------------------------------------------------------
    // AbstractAddThirdPartyMojo Implementaton
    // ----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    protected SortedMap<String, Dependency> loadDependencies()
    {

        SortedMap<String, Dependency> dependencies = new TreeMap<String, Dependency>();

        getLog().info("Update the property \"license.scope\" in the pom file if you want to change it.");

        for (MavenProject mavenProject : reactorProjects)
        {
            Scope scope = getProjectScope(mavenProject);

            getLog().info("Module: " + mavenProject.getName() + "\t - License Scope: " + scope.getPrintableName() + ". ");

            SortedMap<String, Dependency> projDependencies =  getHelper().loadDependencies(mavenProject, this, scope, getIgnoredArtifactsTrees());

            for (Map.Entry<String, Dependency> entry : projDependencies.entrySet())
            {
                String gav = entry.getKey();
                Dependency dependency = entry.getValue();

                Dependency existentDependency = dependencies.get(gav);

                if (existentDependency == null)
                {
                    dependencies.put(gav, dependency);
                }
                else
                {
                    existentDependency.addScopeOnModule(mavenProject, dependency.getLicenseScope());
                }
            }
        }

        return dependencies;
    }

    private Scope getProjectScope(MavenProject mavenProject)
    {
        String scopeConfiguration =(String) mavenProject.getModel().getProperties().get("license.scope");
        Scope scope = Scope.BINARY;
        if (scopeConfiguration != null)
        {
            scope =  Scope.valueOfIgnoreCaseUnrestricted(scopeConfiguration);
        }
        return scope;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SortedProperties createAllOverrideMapping()
            throws Exception, IOException
    {

        String path = getRelativePath(getOverrideFile().getAbsolutePath());

        if ( isVerbose() )
        {
            getLog().info( "Use missing file path : " + path );
        }

        SortedProperties overrideMappings = new SortedProperties( getEncoding() );


        for ( MavenProject project : getChildrenProjects() )
        {
            overrideMappings.putAll(loadOverrideFile(path, project, false));
        }

        overrideMappings.putAll(loadOverrideFile(path, getProject(), true));

        return overrideMappings;
    }


    protected List<File> getNonMavenDependenciesFiles()
    {
        List<File> nonMavenFiles = new ArrayList<File>();
        String relativePath = getRelativePath(getNonMavenDependenciesFile().getAbsolutePath());

        for ( MavenProject project : getChildrenProjects() )
        {
            File file = new File( project.getBasedir(), relativePath );

            if ( file.exists() )
            {
                nonMavenFiles.add(file);
            }
        }

        nonMavenFiles.add(getNonMavenDependenciesFile());
        return nonMavenFiles;
    }

    private List<MavenProject> getChildrenProjects()
    {
        List<MavenProject> childrenProjects = new ArrayList<MavenProject>(reactorProjects);
        childrenProjects.remove(getProject());

        return childrenProjects;
    }

    private String getRelativePath(String absolutePath)
    {
        return absolutePath.substring(getProject().getBasedir().getAbsolutePath().length() + 1);
    }



    private SortedProperties loadOverrideFile(String path, MavenProject mavenProject,
                                              boolean overrideAlways) throws Exception
    {
        File file = new File( mavenProject.getBasedir(), path );

        return getHelper().loadOverrideFile(getLicenseMap(), getProjectDependencies(), getProjectScope(mavenProject), file, mavenProject, overrideAlways);

    }

}
