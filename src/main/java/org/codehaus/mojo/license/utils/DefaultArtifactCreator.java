package org.codehaus.mojo.license.utils;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.wagon.ConnectionException;
import org.apache.maven.wagon.ResourceDoesNotExistException;
import org.apache.maven.wagon.TransferFailedException;
import org.apache.maven.wagon.UnsupportedProtocolException;
import org.apache.maven.wagon.Wagon;
import org.apache.maven.wagon.authentication.AuthenticationException;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.apache.maven.wagon.repository.Repository;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.codehaus.plexus.util.xml.Xpp3DomBuilder;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * @see {@link ArtifactCreator}
 * @plexus.component role="org.codehaus.mojo.license.utils.ArtifactCreator" role-hint="default"
 */
public class DefaultArtifactCreator extends AbstractLogEnabled implements ArtifactCreator
{
    private static final List<String> ATLASSIAN_PLUGIN_PACKAGING_TYPES = Collections.unmodifiableList(
            Arrays.asList("atlassian-plugin", "bundle")
    );
    private static final String SHA1_SEARCH_PATH = "/service/local/data_index?sha1=%s";
    private static final Repository SHA1_SEARCH_REPOSITORY = new Repository("central", "https://maven.atlassian.com/");

    /**
     * @plexus.requirement
     */
    private ArtifactFactory artifactFactory;

    /**
     * @plexus.requirement
     */
    private WagonManager wagonManager;

    // Set in constructor, not injected, purely for testing
    private final TempFileCreator tempFileCreator;

    private Wagon httpWagon;

    DefaultArtifactCreator()
    {
        this.tempFileCreator = new DefaultTempFileCreator();
    }

    DefaultArtifactCreator(ArtifactFactory artifactFactory, WagonManager wagonManager, TempFileCreator tempFileCreator)
    {
        this.artifactFactory = artifactFactory;
        this.wagonManager = wagonManager;
        this.tempFileCreator = tempFileCreator;
    }

    public Artifact fromPom(final InputStream fileInputStream, final String fileName)
    {
        try
        {
            /*Xpp3Dom build = Xpp3DomBuilder.build(new BufferedReader(new InputStreamReader(fileInputStream)));
            Xpp3Dom possibleArtifactId = build.getChild("artifactId");
            Xpp3Dom possibleGroupId = build.getChild("groupId");
            if (possibleGroupId == null) {
                possibleGroupId = build.getChild("parent").getChild("groupId");
            }
            Xpp3Dom possibleVersion = build.getChild("version");
            if (possibleVersion == null) {
                possibleVersion = build.getChild("parent").getChild("version");
            }
            Xpp3Dom possiblePackaging = build.getChild("packaging");
            String possiblePackagingString =  (possiblePackaging == null || ATLASSIAN_PLUGIN_PACKAGING_TYPES.contains(possiblePackaging.getValue())) ?
                    "jar" : possiblePackaging.getValue();

            return artifactFactory.createArtifact(possibleGroupId.getValue(), possibleArtifactId.getValue(),
                    possibleVersion.getValue(), null, possiblePackagingString);*/

            Properties pomProperties = new Properties();
            pomProperties.load(fileInputStream);

            String groupId = (String)pomProperties.get("groupId");
            String artifactId = (String)pomProperties.get("artifactId");
            String version = (String)pomProperties.get("version");
            String packaging = "jar";


            Artifact artifact = artifactFactory.createArtifact(groupId, artifactId,
                    version, null, packaging);

            artifact.setScope("compile");

            return artifact;
        }
        catch (Exception e)
        {
            getLogger().error("An exception occurred when trying to determine the GAV of " + fileName + " via its pom", e);
            return null;
        }
    }

    public Artifact fromSha(final String sha, final String fileName)
    {
        File searchResult = null;
        try
        {
            searchResult = writeSearchResultToFile(sha);
            return getArtifactFromSearchResult(searchResult, fileName);
        }
        catch (Exception e)
        {
            getLogger().error("An exception occurred when trying to determine the GAV of " + fileName + " via its SHA", e);
            return null;
        }
        finally
        {
            if (searchResult != null)
            {
                searchResult.delete();
            }
        }

    }

    private Artifact getArtifactFromSearchResult(final File searchResult, String fileName)
            throws IOException, XmlPullParserException, ArtifactResolutionException, ArtifactNotFoundException
    {
        Xpp3Dom build = Xpp3DomBuilder.build(new BufferedReader(new FileReader(searchResult)));
        Xpp3Dom totalCount = build.getChild("totalCount");
        if (totalCount != null && Integer.parseInt(totalCount.getValue()) > 0 &&
                build.getChild("data").getChildCount() > 0)
        {
            Xpp3Dom artifact = build.getChild("data").getChild(0);
            String groupId = artifact.getChild("groupId").getValue();
            String artifactId = artifact.getChild("artifactId").getValue();
            String version = artifact.getChild("version").getValue();
            Xpp3Dom classifier = artifact.getChild("classifier");
            String classifierString = null;
            if (classifier != null)
            {
                classifierString = classifier.getValue();
            }
            String[] split = fileName.split("\\.");
            String extension = split.length > 0 ? split[split.length - 1] : null;

            Artifact artifactWithClassifier = artifactFactory.createArtifactWithClassifier(groupId, artifactId, version, extension, classifierString);

            artifactWithClassifier.setScope("compile");

            return artifactWithClassifier;
        }
        else
        {
            return null;
        }
    }

    private File writeSearchResultToFile(String sha) throws UnsupportedProtocolException, ConnectionException,
            AuthenticationException, TransferFailedException, ResourceDoesNotExistException,
            AuthorizationException, IOException
    {
        final File searchResult = tempFileCreator.create();
        if (httpWagon == null)
        {
            httpWagon = wagonManager.getWagon(SHA1_SEARCH_REPOSITORY);
        }
        httpWagon.connect(SHA1_SEARCH_REPOSITORY);
        httpWagon.get(String.format(SHA1_SEARCH_PATH, sha), searchResult);
        return searchResult;
    }

    interface TempFileCreator
    {
        public File create() throws IOException;
    }

    class DefaultTempFileCreator implements TempFileCreator
    {
        public File create() throws IOException
        {
            return File.createTempFile("license", null);
        }
    }

}
