package org.codehaus.mojo.license.utils;

import org.apache.maven.artifact.Artifact;

import java.io.InputStream;

/**
 * Creates {@link Artifact}s based on some hint, such as a pom.xml or its SHA-1 hash. Used to determine the
 * actual coordinates of some arbitrarily supplied archive.
 */
public interface ArtifactCreator
{
    /**
     * Creates an {@link Artifact} based on a pom, expected to be supplied via an {@link InputStream}
     *
     * @param inputStream the inputStream created from a pom.xml
     * @param fileName the fileName of the enclosing pom.xml artifact, provided for debugging purposes
     * @return An @{@link Artifact} based on coordinates inferred from the supplied pom
     */
    Artifact fromPom(InputStream inputStream, String fileName);

    /**
     * Creates an {@link Artifact} based on the SHA-1 hash of a file
     *
     * @param sha the SHA-1 hash of the artifact
     * @param fileName the fileName of the SHA-1 hash-ed artifact, provided for debugging purposes
     * @return An @{@link Artifact} based on a search done
     */
    Artifact fromSha(String sha, String fileName);
}
