package org.codehaus.mojo.license.api;

/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.metadata.ArtifactMetadataSource;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactCollector;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;
import org.apache.maven.shared.dependency.tree.DependencyNode;
import org.apache.maven.shared.dependency.tree.DependencyTreeBuilder;
import org.apache.maven.shared.dependency.tree.DependencyTreeBuilderException;
import org.codehaus.mojo.license.model.Dependency;
import org.codehaus.mojo.license.model.MavenDependency;
import org.codehaus.mojo.license.model.Scope;
import org.codehaus.mojo.license.utils.MojoHelper;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.codehaus.plexus.logging.Logger;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Default implementation of the {@link DependenciesTool}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @version $Id: DefaultDependenciesTool.java 16640 2012-05-17 22:44:37Z tchemit $
 * @plexus.component role="org.codehaus.mojo.license.api.DependenciesTool" role-hint="default"
 * @since 1.0
 */
public class DefaultDependenciesTool
    extends AbstractLogEnabled
    implements DependenciesTool
{

    /**
     * Message used when an invalid expression pattern is found.
     */
    public static final String INVALID_PATTERN_MESSAGE =
        "The pattern specified by expression <%s> seems to be invalid.";

    /**
     * Project builder.
     *
     * @plexus.requirement
     */
    private MavenProjectBuilder mavenProjectBuilder;

    /**
     * Helper to resolve nested artifacts in Atlassian artifacts
     *
     * @plexus.requirement
     */
    private AtlassianPluginHelper atlassianPluginHelper;


    /**
     * @plexus.requirement
     */
    private ArtifactMetadataSource artifactMetadataSource;

    /**
     * @plexus.requirement
     */
    private ArtifactCollector artifactCollector;

    /**
     * @plexus.requirement
     */
    private DependencyTreeBuilder treeBuilder;

    /**
     * @plexus.requirement
     */
    private ArtifactFactory artifactFactory;



    /**
     * {@inheritDoc}
     */
    public SortedMap<String, Dependency> loadProjectDependencies(MavenProject project,
                                                                 MavenProjectDependenciesConfigurator configuration,
                                                                 ArtifactRepository localRepository,
                                                                 List<ArtifactRepository> remoteRepositories,
                                                                 Scope moduleScope, Set<String> ignoredTrees)
    {

        if (moduleScope == Scope.IGNORE)
            return new TreeMap<String, Dependency>();


        SortedMap<String,MavenDependency> mavenCache = new TreeMap<String, MavenDependency>();


        boolean haveNoIncludedGroups = StringUtils.isEmpty( configuration.getIncludedGroups() );
        boolean haveNoIncludedArtifacts = StringUtils.isEmpty( configuration.getIncludedArtifacts() );

        boolean haveExcludedGroups = StringUtils.isNotEmpty( configuration.getExcludedGroups() );
        boolean haveExcludedArtifacts = StringUtils.isNotEmpty( configuration.getExcludedArtifacts() );
        boolean haveExclusions = haveExcludedGroups || haveExcludedArtifacts;

        Pattern includedGroupPattern = null;
        Pattern includedArtifactPattern = null;
        Pattern excludedGroupPattern = null;
        Pattern excludedArtifactPattern = null;


        if ( !haveNoIncludedGroups )
        {
            includedGroupPattern = Pattern.compile( configuration.getIncludedGroups() );
        }
        if ( !haveNoIncludedArtifacts )
        {
            includedArtifactPattern = Pattern.compile( configuration.getIncludedArtifacts() );
        }
        if ( haveExcludedGroups )
        {
            excludedGroupPattern = Pattern.compile( configuration.getExcludedGroups() );
        }
        if ( haveExcludedArtifacts )
        {
            excludedArtifactPattern = Pattern.compile( configuration.getExcludedArtifacts() );
        }

        Set<Artifact> depArtifacts;

        if ( configuration.isIncludeTransitiveDependencies() )
        {
            // All project dependencies
            depArtifacts = retrieveDependenciesFromTree(project, localRepository, ignoredTrees);

        }
        else
        {
            // Only direct project dependencies
            depArtifacts = project.getDependencyArtifacts();
        }
        if ( configuration.isResolveNestedAtlassianArtifacts() )
        {
                depArtifacts = atlassianPluginHelper.resolveNestedArtifacts(depArtifacts,
                        remoteRepositories, localRepository, configuration.isVerbose());
        }

        List<String> includedScopes = configuration.getIncludedScopes();
        List<String> excludeScopes = configuration.getExcludedScopes();

        boolean verbose = configuration.isVerbose();

        SortedMap<String, Dependency> result = new TreeMap<String, Dependency>();

        for ( Artifact artifact : depArtifacts )
        {
            String scope = artifact.getScope();
            if ( CollectionUtils.isNotEmpty( includedScopes ) && !includedScopes.contains( scope ) )
            {
                // not in included scopes
                continue;
            }

            if ( excludeScopes.contains( scope ) )
            {
                // in exluced scopes
                continue;
            }

            Logger log = getLogger();

            String id = MojoHelper.getArtifactIdWithScope( artifact );

            if ( verbose )
            {
                log.info( "Detected artifact [" + id  + "]");
            }

            // Check if the project should be included
            // If there is no specified artifacts and group to include, include all
            boolean isToInclude = haveNoIncludedArtifacts && haveNoIncludedGroups ||
                isIncludable( artifact, includedGroupPattern, includedArtifactPattern );

            // Check if the project should be excluded
            boolean isToExclude = isToInclude && haveExclusions &&
                isExcludable( artifact, excludedGroupPattern, excludedArtifactPattern );

            if ( !isToInclude || isToExclude )
            {
                if ( verbose )
                {
                    log.info( "Skipped artifact [" + id + "]" );
                }
                continue;
            }

            MavenDependency depMavenProject = null;

            if ( mavenCache != null )
            {
                // try to get project from mavenCache
                depMavenProject = mavenCache.get( id );
            }


            if ( depMavenProject == null )
            {
                // build project
                try
                {
                    MavenProject mavenProject =
                        mavenProjectBuilder.buildFromRepository( artifact, remoteRepositories, localRepository, true );

                    mavenProject.getArtifact().setScope( artifact.getScope() );

                    depMavenProject = new MavenDependency(project, mavenProject, moduleScope);


                }
                catch ( ProjectBuildingException e )
                {
                    log.warn( "Unable to obtain POM for artifact : " + artifact, e );
                    continue;
                }

                if ( verbose )
                {
                    log.info( "Added dependency [" + id + "]" );
                }
                if ( mavenCache != null )
                {

                    // store it also in mavenCache
                    mavenCache.put( id, depMavenProject );
                }
            }

            // keep the project
            result.put( MojoHelper.getArtifactId(artifact), depMavenProject );
        }
        return result;
    }

    private Set<Artifact> retrieveDependenciesFromTree(final MavenProject project, final ArtifactRepository localRepository, final Set<String> ignoredTrees)
    {

        DependencyNode rootNode = null;
        try
        {
            rootNode = treeBuilder.buildDependencyTree(project, localRepository,
                    artifactFactory, artifactMetadataSource, null, artifactCollector);
        }
        catch (DependencyTreeBuilderException e)
        {
            throw new IllegalArgumentException("The project couldn't generate a dependency tree", e);
        }


        final Set<Artifact> depArtifacts = new HashSet<Artifact>();


        final Queue<DependencyNode> nodesToVisit = new LinkedList<DependencyNode>(rootNode.getChildren());
        DependencyNode actualNode = null;
        while ( (actualNode = nodesToVisit.poll()) != null)
        {
            if (!isNodeIdPresentOnSet(ignoredTrees, actualNode))
            {
                if (actualNode.getState() == DependencyNode.INCLUDED)
                {
                    depArtifacts.add(actualNode.getArtifact());
                }
                nodesToVisit.addAll(actualNode.getChildren());
            }
        }
        return depArtifacts;
    }


    private boolean isNodeIdPresentOnSet(Set<String> artifacts, DependencyNode node)
    {
        Artifact artifact = node.getArtifact();
        String prefix = artifact.getGroupId() + ":" + artifact.getArtifactId();
        return artifacts.contains(prefix);
    }

    /**
     * Tests if the given project is includable against a groupdId pattern and a artifact pattern.
     *
     * @param project                 the project to test
     * @param includedGroupPattern    the include group pattern
     * @param includedArtifactPattern the include artifact pattenr
     * @return {@code true} if the project is includavble, {@code false} otherwise
     */
    protected boolean isIncludable( Artifact project, Pattern includedGroupPattern, Pattern includedArtifactPattern )
    {

        Logger log = getLogger();

        // check if the groupId of the project should be included
        if ( includedGroupPattern != null )
        {
            // we have some defined license filters
            try
            {
                Matcher matchGroupId = includedGroupPattern.matcher( project.getGroupId() );
                if ( matchGroupId.find() )
                {
                    if ( log.isDebugEnabled() )
                    {
                        log.debug( "Include " + project.getGroupId() );
                    }
                    return true;
                }
            }
            catch ( PatternSyntaxException e )
            {
                log.warn( String.format( INVALID_PATTERN_MESSAGE, includedGroupPattern.pattern() ) );
            }
        }

        // check if the artifactId of the project should be included
        if ( includedArtifactPattern != null )
        {
            // we have some defined license filters
            try
            {
                Matcher matchGroupId = includedArtifactPattern.matcher( project.getArtifactId() );
                if ( matchGroupId.find() )
                {
                    if ( log.isDebugEnabled() )
                    {
                        log.debug( "Include " + project.getArtifactId() );
                    }
                    return true;
                }
            }
            catch ( PatternSyntaxException e )
            {
                log.warn( String.format( INVALID_PATTERN_MESSAGE, includedArtifactPattern.pattern() ) );
            }
        }
        return false;
    }

    /**
     * Tests if the given project is excludable against a groupdId pattern and a artifact pattern.
     *
     * @param project                 the project to test
     * @param excludedGroupPattern    the exlcude group pattern
     * @param excludedArtifactPattern the exclude artifact pattenr
     * @return {@code true} if the project is excludable, {@code false} otherwise
     */
    protected boolean isExcludable( Artifact project, Pattern excludedGroupPattern, Pattern excludedArtifactPattern )
    {

        Logger log = getLogger();

        // check if the groupId of the project should be included
        if ( excludedGroupPattern != null )
        {
            // we have some defined license filters
            try
            {
                Matcher matchGroupId = excludedGroupPattern.matcher( project.getGroupId() );
                if ( matchGroupId.find() )
                {
                    if ( log.isDebugEnabled() )
                    {
                        log.debug( "Exclude " + project.getGroupId() );
                    }
                    return true;
                }
            }
            catch ( PatternSyntaxException e )
            {
                log.warn( String.format( INVALID_PATTERN_MESSAGE, excludedGroupPattern.pattern() ) );
            }
        }

        // check if the artifactId of the project should be included
        if ( excludedArtifactPattern != null )
        {
            // we have some defined license filters
            try
            {
                Matcher matchGroupId = excludedArtifactPattern.matcher( project.getArtifactId() );
                if ( matchGroupId.find() )
                {
                    if ( log.isDebugEnabled() )
                    {
                        log.debug( "Exclude " + project.getArtifactId() );
                    }
                    return true;
                }
            }
            catch ( PatternSyntaxException e )
            {
                log.warn( String.format( INVALID_PATTERN_MESSAGE, excludedArtifactPattern.pattern() ) );
            }
        }
        return false;
    }
}
