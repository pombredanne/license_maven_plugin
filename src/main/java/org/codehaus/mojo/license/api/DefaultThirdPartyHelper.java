package org.codehaus.mojo.license.api;

/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2012 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuildingException;
import org.codehaus.mojo.license.model.Dependency;
import org.codehaus.mojo.license.model.LicenseMap;
import org.codehaus.mojo.license.model.LicenseStore;
import org.codehaus.mojo.license.model.MavenDependency;
import org.codehaus.mojo.license.model.NonMavenDependency;
import org.codehaus.mojo.license.model.Scope;
import org.codehaus.mojo.license.utils.SortedProperties;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;

/**
 * Default implementation of the {@link org.codehaus.mojo.license.api.ThirdPartyHelper}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1
 */
public class DefaultThirdPartyHelper
    implements ThirdPartyHelper
{

    /**
     * DependenciesTool to load dependencies.
     *
     * @see DependenciesTool
     */
    private final DependenciesTool dependenciesTool;

    /**
     * ThirdPartyTool to load third-parties descriptors.
     *
     * @see ThirdPartyTool
     */
    private final ThirdPartyTool thirdPartyTool;

    /**
     * Local repository used.
     */
    private final ArtifactRepository localRepository;

    /**
     * List of remote repositories.
     */
    private final List<ArtifactRepository> remoteRepositories;

    /**
     * Current maven project.
     */
    private final MavenProject project;

    /**
     * Encoding used to read and write files.
     */
    private final String encoding;

    /**
     * Verbose flag.
     */
    private final boolean verbose;

    /**
     * Instance logger.
     */
    private final Log log;



    /**
     * Constructor of the helper.
     *
     * @param project            Current maven project
     * @param encoding           Encoding used to read and write files
     * @param verbose            Verbose flag
     * @param dependenciesTool   tool to load dependencies
     * @param thirdPartyTool     tool to load third-parties descriptors
     * @param localRepository    maven local repository
     * @param remoteRepositories maven remote repositories
     * @param log                logger
     */
    public DefaultThirdPartyHelper( MavenProject project, String encoding, boolean verbose,
                                    DependenciesTool dependenciesTool, ThirdPartyTool thirdPartyTool,
                                    ArtifactRepository localRepository, List<ArtifactRepository> remoteRepositories,
                                    Log log )
    {
        this.project = project;
        this.encoding = encoding;
        this.verbose = verbose;
        this.dependenciesTool = dependenciesTool;
        this.thirdPartyTool = thirdPartyTool;
        this.localRepository = localRepository;
        this.remoteRepositories = remoteRepositories;
        this.log = log;

    }


    /**
     * {@inheritDoc}
     */
    public SortedMap<String, Dependency> loadDependencies(MavenProjectDependenciesConfigurator configuration, Scope scope, String ignoredArtifactsTrees)
    {
        return loadDependencies(project, configuration, scope, ignoredArtifactsTrees);
    }


    /**
     * {@inheritDoc}
     */
    public SortedMap<String, Dependency> loadDependencies(MavenProject project, MavenProjectDependenciesConfigurator configuration, Scope scope, String ignoredArtifactsTrees)
    {
        Set<String> ignoredSet = Collections.emptySet();
        if (ignoredArtifactsTrees != null)
        {
            ignoredSet =  new HashSet<String>(Arrays.asList(ignoredArtifactsTrees.split(",")));
        }

        return dependenciesTool.loadProjectDependencies(project, configuration, localRepository, remoteRepositories, scope, ignoredSet);
    }


    /**
     * {@inheritDoc}
     */
    public SortedProperties loadOverrideFileFromRepository(Collection<MavenProject> projects)
        throws ThirdPartyToolException, IOException
    {
        return thirdPartyTool.resolveOverrideFilesFromRepository(encoding, projects, localRepository, remoteRepositories);
    }

    /**
     * {@inheritDoc}
     */
    public SortedProperties loadOverrideFile(LicenseMap licenseMap, Map<String, Dependency> mavenCache, Scope scope, File missingFile, MavenProject project, boolean overrideAlways)
        throws IOException
    {
        return thirdPartyTool.loadOverrideFile(licenseMap, scope, mavenCache, encoding, missingFile, project, overrideAlways);
    }


    public void pickMostPermissiveLicenses(LicenseMap licenseMap, String missingFilePath)
    {
        List<String> licensePatterns = LicenseStore.getLicensesPatterns();

        SortedMap<Dependency, String[]> licensesPerDependency =  licenseMap.getLicensesPerDependency();

        for (Map.Entry<Dependency, String[]> entry : licensesPerDependency.entrySet())
        {
            Dependency dependency = entry.getKey();
            String licenses[] = entry.getValue();

            if (licenses.length > 1)
            {
                String license = retrieveMostPermissiveLicenseFromList(dependency, licensePatterns, licenses, missingFilePath);
                log.info("Artifact " + dependency.getGAVInfo()+ " : picking license " + license + " from [" + StringUtils.join(licenses, ",") + "]" );
                licenseMap.removeOtherLicensesOfDependency(dependency, license);
            }
        }
    }

    protected String retrieveMostPermissiveLicenseFromList(Dependency dependency,  List<String> licensePatterns, String[] licenses, String missingFilePath)
    {
        for (String licensePattern : licensePatterns)
        {
            for (String license : licenses)
            {
                if (licensePattern.equalsIgnoreCase(license))
                {
                    return license;
                }
            }
        }

        StringBuilder errorMessage = new StringBuilder("\n\n\nArtifact ");
        errorMessage.append(dependency.getArtifactName());
        errorMessage.append(" has multiple licenses:").append("\n");

        int i = 1;
        for (String license : licenses)
        {
            errorMessage.append(i).append(". ").append(license).append("\n");
            i++;
        }
        errorMessage.append("\nIt was not possible to automatically select the most appropriate license to use.");
        errorMessage.append("\n\nPlease specify which license to use by creating an entry for ").append(dependency.getArtifactName());
        errorMessage.append(" in " + missingFilePath + " file.\n\n");
        log.error(errorMessage);

        throw new IllegalArgumentException(" It was not possible to automatically select a license for artifact "+ dependency.getArtifactName()
                +". Please consult the error message above for more details.");
    }

    /**
     * {@inheritDoc}
     */
    public LicenseMap createLicenseMap(SortedMap<String, Dependency> dependencies, String encoding, Scope scope, List<String> excludedGAVs, MavenProject mavenProject) throws IOException
    {

        LicenseMap licenseMap = new LicenseMap();

        for ( Dependency dependency : dependencies.values() )
        {
            if (!excludedGAVs.contains(dependency.getGAVInfo()))
            {
                thirdPartyTool.addLicense(licenseMap, dependency, dependency.getLicenses());
            }
        }




        return licenseMap;
    }


    public void loadNonMavenDependencies(List<File> nonMavenDependenciesFiles, String encoding, MavenProject mavenProject, LicenseMap licenseMap) throws IOException
    {
        if (nonMavenDependenciesFiles != null)
        {
            Map<String, NonMavenDependency> allNonMavenDeps = new TreeMap<String, NonMavenDependency>();


            for (File nonMavenDependenciesFile : nonMavenDependenciesFiles)
            {
                Set<NonMavenDependency> nonMavenDependencySet = thirdPartyTool.createNonMavenDependencies(nonMavenDependenciesFile, encoding, mavenProject);

                for (NonMavenDependency nonMavenDependency : nonMavenDependencySet)
                {
                    NonMavenDependency existentDep = allNonMavenDeps.get(nonMavenDependency.getId());

                    //get previous scope values
                    if (existentDep != null)
                    {
                        nonMavenDependency.addScopeOnModule(existentDep.getScopesPerModule());
                    }

                    allNonMavenDeps.put(nonMavenDependency.getId(), nonMavenDependency);
                }
            }


            for (NonMavenDependency nonMavenDependency : allNonMavenDeps.values())
            {
                thirdPartyTool.addLicense(licenseMap, nonMavenDependency, nonMavenDependency.getLicense());
            }
        }
    }


    /**
     * {@inheritDoc}
     */
    public void attachThirdPartyDescriptor( File file )
    {

        thirdPartyTool.attachThirdPartyDescriptor( project, file );
    }


    /**
     * {@inheritDoc}
     */
    public SortedSet<Dependency> getProjectsWithNoLicense( LicenseMap licenseMap )
    {
        return thirdPartyTool.getProjectsWithNoLicense( licenseMap, verbose );
    }

    /**
     * {@inheritDoc}
     */
    public SortedSet<MavenProject> getMavenProjectsWithNoLicense( LicenseMap licenseMap )
    {
        return thirdPartyTool.getMavenProjectsWithNoLicense( licenseMap, verbose );
    }

    /**
     * {@inheritDoc}
     */
    public SortedProperties createOverrideMappingForProject(LicenseMap licenseMap, Scope scope,
                                                            File missingFile,
                                                            boolean useRepositoryMissingFiles,
                                                            Map<String, Dependency> projectDependencies,
                                                            MavenProject project)
        throws ProjectBuildingException, IOException, ThirdPartyToolException
    {

        SortedProperties overrideMappings = loadOverrideFile(licenseMap, projectDependencies, scope, missingFile, project, false);


        if ( useRepositoryMissingFiles )
        {
            // try to load missing third party files from dependencies
            Collection<MavenProject> projects = new ArrayList<MavenProject>();

            for (Dependency dependency : projectDependencies.values())
            {
                if (dependency instanceof MavenDependency)
                {
                    projects.add(((MavenDependency)dependency).getMavenProject());

                }
            }
            projects.remove( project );
            overrideMappings.putAll(loadOverrideFileFromRepository(projects)) ;
        }

        return overrideMappings;
    }

    /**
     * {@inheritDoc}
     */
    public void mergeLicenses( List<String> licenseMerges, LicenseMap licenseMap )
        throws MojoFailureException
    {

        if ( !CollectionUtils.isEmpty( licenseMerges ) )
        {

            // check where is not multi licenses merged main licenses (see OJO-1723)
            Map<String, String[]> mergedLicenses = new HashMap<String, String[]>();

            for ( String merge : licenseMerges )
            {
                merge = merge.trim();
                String[] split = merge.split( "\\|" );

                String mainLicense = split[0];
                List<String> derivedLicenses = Arrays.asList(Arrays.copyOfRange(split, 1, split.length));

                if ( mergedLicenses.containsKey( mainLicense ) || derivedLicenses.contains(mainLicense))
                {

                    // this license was already describe, fail the build...

                    throw new MojoFailureException(
                        "The merge main license " + mainLicense + " was already registred in the " +
                            "configuration, please use only one such entry as describe in example " +
                            "http://mojo.codehaus.org/license-maven-plugin/examples/example-thirdparty.html#Merge_licenses." );
                }
                mergedLicenses.put( mainLicense, split );
            }

            // merge licenses in license map

            for ( String[] mergedLicense : mergedLicenses.values() )
            {
                if ( verbose )
                {
                    log.info( "Will merge " + Arrays.toString( mergedLicense ) + "" );
                }

                thirdPartyTool.mergeLicenses( licenseMap, mergedLicense );
            }
        }
    }


    public void removeIgnoredLicenses(LicenseMap licenseMap, String ignoredLicenses)
    {
        licenseMap.removeLicenses(Arrays.asList(ignoredLicenses.split(",")));
    }



    public String pluralise(int count, String single, String plural) {
        if (count > 1) {
            return plural;
        } else {
            return single;
        }
    }

}
