package org.codehaus.mojo.license.api;

import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.file.TFileInputStream;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.metadata.ArtifactMetadataRetrievalException;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.AbstractArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.project.ProjectBuildingException;
import org.codehaus.mojo.license.model.CandidateArtifact;
import org.codehaus.mojo.license.utils.ArtifactCreator;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.codehaus.plexus.util.IOUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Default implementation of the {@link AtlassianPluginHelper}.
 *
 * @plexus.component role="org.codehaus.mojo.license.api.AtlassianPluginHelper" role-hint="default"
 * @since 1.3-atlassian-1.4
 */
public class DefaultAtlassianPluginHelper extends AbstractLogEnabled
        implements AtlassianPluginHelper {

    /**
     * @plexus.requirement
     */
    private ArtifactResolver resolver;

    /**
     * @plexus.requirement
     */
    private ArtifactCreator artifactCreator;

    private static final Map<String, Artifact> shaToArtifactCache = new HashMap<String, Artifact>();
    private static final Set<String> unknownArtifactCache = new HashSet<String>();

    private static final String ATLASSIAN_GROUPID_PREFIX = "com.atlassian";

    DefaultAtlassianPluginHelper()
    {
    }

    DefaultAtlassianPluginHelper(final ArtifactResolver resolver,
                                 final ArtifactCreator artifactCreator)
    {
        this.resolver = resolver;
        this.artifactCreator = artifactCreator;
    }

    /**
     * @see AtlassianPluginHelper#resolveNestedArtifacts
     */
    public Set<Artifact> resolveNestedArtifacts(final Set<Artifact> artifacts,
                                                final List<ArtifactRepository> remoteRepositories,
                                                final ArtifactRepository localRepository, boolean verbose)
    {

        // Don't swallow any exceptions when resolving nested artifacts. If an exception happens when
        // resolving dependencies, it's probably something we should find out, and resolution should
        // not continue.
        try
        {
            return resolveNestedArtifactsInternally(artifacts, remoteRepositories, localRepository, verbose);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public boolean isAtlassianArtifact(final Artifact artifact)
    {
        return artifact != null && artifact.getGroupId() != null &&
                artifact.getGroupId().contains(ATLASSIAN_GROUPID_PREFIX);
    }

    private Set<Artifact> resolveNestedArtifactsInternally(final Set<Artifact> artifacts,
                                                           final List<ArtifactRepository> remoteRepositories,
                                                           final ArtifactRepository localRepository, boolean verbose)
            throws ArtifactMetadataRetrievalException, ProjectBuildingException, ArtifactResolutionException,
                ArtifactNotFoundException
    {

        final Set<Artifact> collectedArtifacts = new HashSet<Artifact>(artifacts);

        // Map from archive filename -> candidate artifact
        final Map<String, CandidateArtifact> fileToArtifact = collectNestedAtlassianArtifacts(artifacts, remoteRepositories, localRepository);

        for (final Map.Entry<String, CandidateArtifact> fileToArtifactEntry : fileToArtifact.entrySet())
        {
            final CandidateArtifact candidateArtifact = fileToArtifactEntry.getValue();
            final String candidateArtifactSha = candidateArtifact.getSha();

            final Artifact cacheValue = shaToArtifactCache.get(candidateArtifactSha);

            if (unknownArtifactCache.contains(candidateArtifactSha))
            {
                continue;
            }
            if (cacheValue != null)
            {
                collectedArtifacts.add(cacheValue);
                continue;
            }

            getLogger().debug(candidateArtifact.toString());
            if (!candidateArtifact.getContainedArtifacts().isEmpty())
            {
                final List<Artifact> containedArtifacts = candidateArtifact.getContainedArtifacts();
                for (Artifact containedArtifact : containedArtifacts)
                {
                    try
                    {
                        resolver.resolve(containedArtifact, remoteRepositories, localRepository);

                        if (verbose)
                        {
                            getLogger().info("Nested jar identified " + candidateArtifact.getFileName() + " as [" + containedArtifact + "] ");
                        }
                    }
                    catch (AbstractArtifactResolutionException e)
                    {
                        if (verbose)
                        {
                            getLogger().warn("Unable to resolve " + containedArtifact + ", its pom.xml metadata " +
                                "may have been malformed.");
                        }
                    }
                }
                collectedArtifacts.addAll(containedArtifacts);
            }
            else
            {
                Artifact artifact = artifactCreator.fromSha(candidateArtifactSha, candidateArtifact.getFileName());
                if (artifact != null)
                {
                    shaToArtifactCache.put(candidateArtifactSha, artifact);
                    resolver.resolve(artifact, remoteRepositories, localRepository);
                    collectedArtifacts.add(artifact);
                }
                else
                {
                    unknownArtifactCache.add(candidateArtifactSha);
                    getLogger().error("\tUnable to resolve artifact " + candidateArtifact.getFileName() + " originating from "
                            + candidateArtifact.getFullPath() + " (looking for nested jars)");
                }
            }

        }
        return collectedArtifacts;
    }

    private Map<String, CandidateArtifact> collectNestedAtlassianArtifacts(final Set<Artifact> artifacts, List<ArtifactRepository> remoteRepositories, ArtifactRepository localRepository) throws ArtifactResolutionException, ArtifactNotFoundException
    {
        final Map<String, CandidateArtifact> fileToArtifact = new HashMap<String, CandidateArtifact>();

        for (final Artifact artifact : artifacts)
        {
            if (isAtlassianArtifact(artifact))
            {
                if (artifact.getFile() == null)
                {
                    try
                    {
                        resolver.resolve(artifact, remoteRepositories, localRepository);
                        getLogger().debug("Resolving " + artifact.getId());
                    }
                    catch (AbstractArtifactResolutionException e)
                    {
                        getLogger().error("\tUnable to resolve dependency" + artifact.getId() + " (looking for nested jars)");
                    }
                }
                collectNestedArtifactsRecursively(artifact.getArtifactId(), new TFile(artifact.getFile()), fileToArtifact);

            }
        }
        return fileToArtifact;
    }

    private void collectNestedArtifactsRecursively(final String artifactId, final TFile tFile,
                                           final Map<String, CandidateArtifact> fileToArtifact)
    {
        TFile[] tFiles = tFile.listFiles();
        if (tFiles != null)
        {
            for (TFile archiveFile : tFiles)
            {
                final String fileName = archiveFile.getName();
                if (isArchive(fileName))
                {
                    getLogger().debug(artifactId + " contains " + archiveFile.getAbsolutePath());

                    final String sha = getShaForFile(archiveFile, fileName);
                    getLogger().debug(archiveFile.getAbsolutePath() + " sha " + sha);

                    fileToArtifact.put(fileName, new CandidateArtifact(sha, fileName, archiveFile.getAbsolutePath()));
                    collectNestedArtifactsRecursively(artifactId, archiveFile, fileToArtifact);
                }
                else if (archiveFile.isDirectory())
                {
                    collectNestedArtifactsRecursively(artifactId, archiveFile, fileToArtifact);
                }
                else if (isMetadataPomFile(archiveFile, fileName))
                {
                    TFileInputStream tFileInputStream = null;
                    try
                    {
                        tFileInputStream = new TFileInputStream(archiveFile.toNonArchiveFile());
                        final String enclosingArchiveFileName = archiveFile.getEnclArchive().getName();
                        final Artifact artifact = artifactCreator.fromPom(tFileInputStream, enclosingArchiveFileName);

                        final CandidateArtifact candidateArtifact = fileToArtifact.get(enclosingArchiveFileName);
                        if (candidateArtifact != null)
                        {
                            candidateArtifact.addContainedArtifact(artifact);
                        }
                    }
                    catch (Exception e)
                    {
                        getLogger().error("An error occurred when trying to extract metadata from " + archiveFile, e);
                    }
                    finally
                    {
                        IOUtil.close(tFileInputStream);
                    }
                }
            }
        }
    }

    private boolean isMetadataPomFile(final TFile archiveFile, final String fileName)
    {
        return fileName.equals("pom.properties") && archiveFile.getAbsolutePath().contains("META-INF");
    }

    private boolean isArchive(String fileName)
    {
        return fileName.endsWith(".jar") || fileName.endsWith(".zip");
    }

    private String getShaForFile(final TFile archiveFile, final String fileName)
    {
        TFileInputStream tFileInputStream = null;
        try
        {
            tFileInputStream = new TFileInputStream(archiveFile.toNonArchiveFile());
            return DigestUtils.shaHex(tFileInputStream);
        }
        catch (IOException e)
        {
            getLogger().error("An error occurred generating the sha for " + fileName, e);
        }
        finally
        {
            IOUtil.close(tFileInputStream);
        }
        return null;
    }

}