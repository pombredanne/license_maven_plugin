package org.codehaus.mojo.license;


import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;

import java.io.File;
import java.util.Arrays;

@Mojo( name = "run", requiresProject = true, aggregator = true)
public class AggregateGoals extends AbstractLicenseMojo
{

    @Override
    protected void init() throws Exception
    {

    }

    @Override
    protected void doAction() throws Exception
    {
        final Invoker invoker = new DefaultInvoker();
        invoker.setLocalRepositoryDirectory(new File(getSession().getLocalRepository().getBasedir()));

        callGoal(invoker, "generate-bom", "generate BOM file");
        callGoal(invoker, "files-check", "do the license files check");
    }



    private void callGoal(final Invoker invoker, final String goal, final String description) throws MavenInvocationException
    {
        final InvocationRequest request = new DefaultInvocationRequest();
        request.setGoals(Arrays.asList("org.codehaus.mojo:license-maven-plugin:" + goal) );


        getLog().info("\n\n\n------------- [ "+goal+" - STARTING ] --------------\n\n\n");
        final InvocationResult result = invoker.execute( request );

        if ( result.getExitCode() == 0 )
        {
            getLog().info("\n\n\n------------- [ "+goal+" - SUCCESS ] --------------\n\n\n");
        }
        else
        {
            throw new IllegalStateException( "Failed to "+ description +". See messages above. " );
        }


    }
}
