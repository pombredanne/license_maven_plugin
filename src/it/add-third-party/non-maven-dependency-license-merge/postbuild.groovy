/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

file = new File(basedir, 'target/generated-sources/license/THIRD-PARTY.txt');
assert file.exists();
content = file.text;
assert !content.contains("The Apache Software License, Version 2.0");
assert content.contains("jqueryjs,1.7,MIT License,http://jquery.com/");
assert content.contains("jquery-hashchange,1.3,MIT License,http://github.com/cowboy/jquery-hashchange/");
assert content.contains("Commons Logging,commons-logging:commons-logging:jar:1.1.1,A beautiful license,http://commons.apache.org/logging");
assert content.contains("JHLabs Image Processing Filters,com.jhlabs:filters:jar:2.0.235,A beautiful license,http://www.jhlabs.com/ip/index.html");
return true;
