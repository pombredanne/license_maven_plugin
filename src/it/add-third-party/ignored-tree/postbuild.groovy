/*
 * #%L
 * License Maven Plugin
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin, Codehaus, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

file = new File(basedir, 'target/generated-sources/license/THIRD-PARTY.txt')
assert file.exists()
content = file.text
assert content.contains("Component, GAV, License, URL, Scope")

assert content.contains("commons-logging:commons-logging")

// hamcrest is a dependency of mockito-core and our project pom
assert content.contains("org.hamcrest:hamcrest-core")

// ignored by configuration
assert !content.contains("org.mockito:mockito-core")

// transitive dependency of mockito-core
assert !content.contains("org.objenesis:objenesis")

// jsr305 is not a dependency on the project, even if it's ignored on the config
assert !content.contains("com.google.code.findbugs:jsr305")


return true;

