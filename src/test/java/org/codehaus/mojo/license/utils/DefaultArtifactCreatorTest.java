package org.codehaus.mojo.license.utils;

import junit.framework.TestCase;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.manager.WagonConfigurationException;
import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.wagon.UnsupportedProtocolException;
import org.apache.maven.wagon.Wagon;
import org.apache.maven.wagon.repository.Repository;
import org.codehaus.plexus.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.InputStream;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Test for {@link DefaultArtifactCreator}
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultArtifactCreatorTest extends TestCase
{
    private DefaultArtifactCreator artifactCreator;

    @Mock
    private WagonManager wagonManager;

    @Mock
    private ArtifactFactory artifactFactory;

    @Mock
    private DefaultArtifactCreator.TempFileCreator creator;

    @Mock
    private Wagon wagon;

    @Mock
    private Logger logger;

    @Before
    public void setUp() throws UnsupportedProtocolException, WagonConfigurationException
    {
        this.artifactCreator = new DefaultArtifactCreator(artifactFactory, wagonManager, creator);
        this.artifactCreator.enableLogging(logger);
    }

    @Test
    public void testFromPomWithResult() throws Exception
    {
        final InputStream pomFile = getClass().getResourceAsStream("somePom.properties");
        final Artifact fakeArtifact = mock(Artifact.class);
        when(artifactFactory.createArtifact(
                eq("com.test"),
                eq("testplugin"),
                eq("1.1-SNAPSHOT"),
                isNull(String.class),
                eq("jar"))).thenReturn(fakeArtifact);

        final Artifact artifactFromPom = artifactCreator.fromPom(pomFile, "filename.jar");
        assertSame(fakeArtifact, artifactFromPom);
    }

    @Test
    public void testFromPomWithNoResult() throws Exception
    {
        final InputStream pomFile = getClass().getResourceAsStream("malformedPom.properties");

        final Artifact artifactFromPom = artifactCreator.fromPom(pomFile, "filename.jar");
        assertNull(artifactFromPom);
    }

    @Test
    public void testFromShaNoResult() throws Exception
    {
        final String testSha = "abcdef";

        File searchResult = new File(getClass().getResource("noSearchResults.xml").getFile());
        when(wagonManager.getWagon(any(Repository.class))).thenReturn(wagon);
        when(creator.create()).thenReturn(searchResult);

        final Artifact artifactFromSha = artifactCreator.fromSha(testSha, "filename");

        verify(wagon).get(contains(testSha), eq(searchResult));
        verify(wagon).connect(any(Repository.class));
        assertNull(artifactFromSha);

    }

    @Test
    public void testFromShaWithResult() throws Exception
    {
        final String testSha = "abcdef";

        File searchResult = new File(getClass().getResource("someSearchResults.xml").getFile());
        Artifact fakeArtifact = mock(Artifact.class);
        when(wagonManager.getWagon(any(Repository.class))).thenReturn(wagon);
        when(creator.create()).thenReturn(searchResult);

        // should correspond to someSearchResults.xml
        when(artifactFactory.createArtifactWithClassifier(
                eq("com.atlassian.security"),
                eq("atlassian-cookie-tools"),
                eq("3.1.4-m1"),
                eq("jar"),
                isNull(String.class))).thenReturn(fakeArtifact);

        final Artifact artifactFromSha = artifactCreator.fromSha(testSha, "filename.jar");

        verify(wagon).get(contains(testSha), eq(searchResult));
        verify(wagon).connect(any(Repository.class));

        assertSame(fakeArtifact, artifactFromSha);
    }
}
